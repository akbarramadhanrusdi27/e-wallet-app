import 'package:bankapp/ui/pages/home_page.dart';
import 'package:bankapp/ui/pages/onboarding_page.dart';
import 'package:bankapp/ui/pages/sign_in_page.dart';
import 'package:bankapp/ui/pages/sign_up_page.dart';
import 'package:bankapp/ui/pages/sign_up_set_ktp_page.dart';
import 'package:bankapp/ui/pages/sign_up_set_profile_page.dart';
import 'package:bankapp/ui/pages/sign_up_success_page.dart';
import 'package:bankapp/ui/pages/splash_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => const SplashPage(),
        '/onboarding': (context) => OnboardingPage(),
        '/sign-in': (context) => SignInPage(),
        '/sign-up': (context) => SignUpPage(),
        '/sign-up-set-profile': (context) => SignUpSetProfilePage(),
        '/sign-up-set-ktp': (context) => SignUpSetKtpPage(),
        '/sign-up-success': (context) => SignUpSuccessPage(),
        '/home': (context) => HomePage(),
      },
    );
  }
}
